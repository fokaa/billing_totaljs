framework.helpers.sum = function sumAccess(data, consumerKey, apiId, month, year){;
        var sum = 0;
        for(var i = 0; i < data.length; i++){
            var doc = data[i];
            if ((doc.values.consumerKey == consumerKey) && (doc.values.api_version == apiId) && (parseInt(doc.values.month,10) == month) && (parseInt(doc.values.year,10) == year)){
                sum += parseInt(doc.values.total_request_count,10);
            }
        }
        return sum==0?"":sum;
}

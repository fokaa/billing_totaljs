const HEADER = { "Authorization" : "Basic " + (new Buffer(CONFIG('api.username')+ ":" + CONFIG('api.password'))).toString('base64')};
const MESIACE = [{n:"Január",v:1},{n:"Február",v:2},{n:"Marec",v:3},{n:"Apríl",v:4},{n:"Máj",v:5},{n:"Jún",v:6},{n:"Júl",v:7},{n:"August",v:8},{n:"September",v:9},{n:"Október",v:10},{n:"November",v:11},{n:"December",v:12}];

exports.apiusage = function(query, callback) {
    var self = this;

    var date = new Date();
    var mesiac = date.getMonth() + 1;
    var rok = date.getUTCFullYear();

    if (query){
        var queryUrl = query.split('&');
        for(var i = 0; i < queryUrl.length; i++){
            var q = queryUrl[i].split('=');
            if (q[0] == 'year') rok = parseInt(q[1],10);
            if (q[0] == 'month') mesiac = parseInt(q[1],10);
        }
    }

    function getMonths(year, roky){
        var mesiacTemp = [];
        for (i in roky[year].mesiace){
            mesiacTemp.push(MESIACE[i-1]);
        }
        return mesiacTemp;
    }

    Utils.request(CONFIG('api.url'),{},{},function(err,data,status,headers){
        if (status == 200){
            var users = [];
            var apis = [];
            var roky = [];
            var roky2 = {};
            var usersObj = {};
            var apisObj = {};
            var rokObj = {};

            data = JSON.parse(data);

            for ( var i = 0; i < data.length; i++ ){
                var item = data[i];
                if (!usersObj[item.values.userId]) {
                    usersObj[item.values.userId] = item.values.userId;
                    users.push({ userId: item.values.userId, userKey:  item.values.consumerKey});
                }
                users.sort(function(a,b){return a.userId.localeCompare(b.userId)});
                if (!apisObj[item.values.api_version]) {
                    apisObj[item.values.api_version] = item.values.api_version;
                    apis.push({api:item.values.api_version});
                }
                apis.sort(function(a,b){return a.api.localeCompare(b.api)});
                var year = parseInt(item.values.year,10);
                if (!rokObj[year]) {
                    rokObj[year] = year;
                    roky.push(year);
                }
                var month = parseInt(item.values.month,10);
                if (!roky2[year]){
                    roky2[year] = {mesiace:{}};
                } else if (!roky2[year].mesiace[month]) {
                    roky2[year].mesiace[month] = month;
                }

            };

            callback(null, {users:users, apis:apis, data:data, mesiace:getMonths(year, roky2), roky:roky, query:{rok:rok,mesiac:mesiac}});
        } else {
            callback({status:status, err:err}, null);
        }
    },{}, HEADER );
}

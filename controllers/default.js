

exports.install = function() {
	F.route('/', view_billing);;
};

function view_billing() {
    var self = this;

    MODEL('apiusage').apiusage(self.req.uri.query, function(err,data){
        if (data)
            self.view("billing",data);
        else
            self.view('error',err);
    });
}
